<?php

namespace Drupal\bootstrap_components_toolkit_ex_icons\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\bootstrap_components_toolkit\Plugin\Field\FieldFormatter\BootstrapComponentsToolkitLinkButtonFormatter;

/**
 * Plugin implementation of the 'Bootstrap Link Button' formatter.
 *
 * @FieldFormatter(
 *   id = "bootstrap_components_toolkit_ex_icons_bootstrap_link_button",
 *   label = @Translation("Bootstrap Link Button + Ex Icons"),
 *   field_types = {
 *     "link",
 *     "email",
 *     "file"
 *   }
 * )
 */
class BootstrapComponentsToolkitExIconsLinkButtonFormatter extends BootstrapComponentsToolkitLinkButtonFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    // TODO: Allow the user to select icon position, size and margin.
    return [
      'icon' => FALSE
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements = parent::settingsForm($form, $form_state);

    $elements['icon'] = [
      '#type' => 'ex_icon_select',
      '#title' => $this->t('Icon'),
      '#default_value' => $this->getSetting('icon'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Icon: @icon', ['@icon' => $this->getSetting('icon') ? $this->getSetting('icon') : 'N/A']);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#theme' => 'bootstrap_button',
        '#type' => (bool) $this->getSetting('type') ? $this->getSetting('type') : FALSE,
        '#color' => (bool) $this->getSetting('color') ? $this->getSetting('color') : FALSE,
        '#size' => (bool) $this->getSetting('size') ? $this->getSetting('size') : FALSE,
        '#button_content' => $this->getButtonIconContent($item),
        '#url' => $this->getButtonUrl($item),
      ];
    }

    return $element;
  }

  /**
   * Creates a render array from the button text and the selected icon.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item.
   *
   * @return string
   *   The button content as a render array.
   */
  protected function getButtonIconContent(FieldItemInterface $item): array {
    return [
      'text' => [
        '#markup' => $this->getButtonContent($item),
      ],
      'icon' => [
        '#theme' => 'ex_icon',
        '#id' => $this->getSetting('icon'),
        '#attributes' => [
          'width' => '1em',
          'height' => '1em',
          'class' => [
            'btn-icon',
            'ms-1'
          ]
        ],
      ]
    ];
  }

}

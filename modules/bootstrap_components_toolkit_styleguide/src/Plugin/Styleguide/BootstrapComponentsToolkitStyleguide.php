<?php

namespace Drupal\bootstrap_components_toolkit_styleguide\Plugin\Styleguide;

use Drupal\Core\Url;
use Drupal\bootstrap_components_toolkit\BootstrapComponentsToolkitColor;
use Drupal\styleguide\GeneratorInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\styleguide\Plugin\StyleguidePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Bootstrap Style guide items implementation.
 *
 * @Plugin(
 *   id = "bootstrap_components_toolkit_styleguide",
 *   label = @Translation("Bootstrap Components Toolkit - Style guide elements")
 * )
 */
class BootstrapComponentsToolkitStyleguide extends StyleguidePluginBase {

  /**
   * The styleguide generator service.
   *
   * @var \Drupal\styleguide\Generator
   */
  protected $generator;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new StcStyleguide.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\styleguide\GeneratorInterface $styleguide_generator
   *   Styleguide generator.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GeneratorInterface $styleguide_generator, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->generator = $styleguide_generator;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('styleguide.generator'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function items() {
    $items = [];

    $items += $this->bsSpacers();
    $items += $this->bsButtons();
    $items += $this->bsAlert();
    $items += $this->bsBadge();
    $items += $this->bsDropdown();
    $items += $this->bsCard();
    $items += $this->bsTabs();
    $items += $this->bsAccordion();
    $items += $this->bsFigure();

    return $items;
  }

  /**
   * Spacer size visualizer.
   *
   * This is not a component but a bootstrap setting, but has a great impact
   * on the overall apearance and visualizing it is very useful for design.
   *
   * @return array
   *   Items with new elements.
   */
  private function bsSpacers() {
    $items = [];

    $items['bootstrap_spacers'] = [
      'title' => $this->t('Spacers'),
      'content' => [
        '#type' => "container",
        '#attributes' => [
          'class' => [
            'row',
            'align-items-end'
          ]
        ]
      ],
      'group' => $this->t('Bootstrap Components Toolkit'),
    ];

    for($i = 0; $i < 12; $i++) {
      $items['bootstrap_spacers']['content'][$i] = [
        '#type' => "container",
        '#attributes' => ['class' => ['col-12 col-sm-4 col-md-3 mb-3']],
        'container' => [
          '#type' => "container",
          'spacer' => [
            '#type' => "container",
            '#attributes' => [
              'class' => [
                'pt-' . $i,
                'pe-' . $i,
                'bg-primary',
                'd-inline-block'
              ]
            ],
          ],
        ],
        'text' => [
          '#markup' => "<code>map-get(\$spacer, $i)</code>"
        ]
      ];
    }
    return $items;
  }

  /**
   * Buttons component generator.
   *
   * @return array
   *   Items with new elements.
   */
  private function bsDropdown() {

    $items = [];

    $types = [
      'default' => 'Default',
      'outline' => 'Outline',
    ];

    $colors = BootstrapComponentsToolkitColor::getButtonScheme();

    $link_items = [
      [
        '#type' => 'link',
        '#title' => $this->generator->words(4),
        '#url' => Url::fromRoute('<front>'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->generator->words(4),
        '#url' => Url::fromRoute('<front>'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->generator->words(4),
        '#url' => Url::fromRoute('<front>'),
      ],
    ];

    $items['bootstrap_dropdown'] = [
      'title' => $this->t('Dropdown'),
      'content' => ['#type' => 'container'],
      'group' => $this->t('Bootstrap Components Toolkit'),
    ];

    foreach ($types as $type_key => $type) {
      $items['bootstrap_dropdown']['content'][$type_key] = [
        'title' => [
          '#markup' => "<h6>{$type}</h6>",
        ],
        '#type' => 'container',
        '#attributes' => ['class' => ['mb-5']],
      ];
      foreach ($colors as $color) {
        $items['bootstrap_dropdown']['content'][$type_key][$color] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['mb-2']],
        ];

        $items['bootstrap_dropdown']['content'][$type_key][$color]["{$type_key}_{$color}"] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'd-flex',
            ],
          ],
        ];
        $dropdown_array = [
          '#theme' => 'bootstrap_dropdown',
          '#title' => "Dropdown {$type_key} {$color}",
          '#type' => $type_key === 'default' ? FALSE : $type_key,
          '#color' => $color,
          '#attributes' => [
            'class' => [
              'mb-2',
              'mr-2',
            ],
          ],
          '#items' => $link_items,
        ];

        $items['bootstrap_dropdown']['content'][$type_key][$color]["{$type_key}_{$color}"]['default'] = $dropdown_array;

        // Alternative version , with dropdowns.
        $dropdown_array_alt = $dropdown_array;
        $dropdown_array_alt['#url'] = Url::fromRoute('<front>');
        $items['bootstrap_dropdown']['content'][$type_key][$color]["{$type_key}_{$color}"]["alt"] = $dropdown_array_alt;

      }
    };

    // Variation examples.
    $items['bootstrap_dropdown']['content']['variations'] = [
      'title' => [
        '#markup' => "<h5>Variations</h5>",
      ],
      '#type' => 'container',
      '#attributes' => ['class' => ['mb-2']],
    ];

    // 1 - Size
    $sizes = [
      'sm',
      'default',
      'lg',
    ];

    $items['bootstrap_dropdown']['content']['variations']['size'] = [
      'title' => [
        '#markup' => "<h6>Size</h6>",
      ],
      '#type' => 'container',
      '#attributes' => ['class' => ['mb-5']],
    ];

    foreach ($sizes as $size) {
      $items['bootstrap_dropdown']['content']['variations']['size'][$size] = [
        '#theme' => 'bootstrap_dropdown',
        '#title' => "Dropdown {$size}",
        '#id' => 'id-' . rand(100, 10000),
        '#size' => $size,
        '#attributes' => [
          'class' => [
            'mb-2',
          ],
        ],
        '#items' => $link_items,
      ];
    }

    // 2 - Directions
    $directions = [
      FALSE,
      'up',
      'end',
      'start',
    ];

    $items['bootstrap_dropdown']['content']['variations']['direction'] = [
      'title' => [
        '#markup' => "<h6>Direction</h6>",
      ],
      '#type' => 'container',
      '#attributes' => ['class' => ['mb-5']],
    ];

    foreach ($directions as $dir) {
      $items['bootstrap_dropdown']['content']['variations']['direction'][$dir] = [
        '#theme' => 'bootstrap_dropdown',
        '#title' => "Drop{$dir}",
        '#direction' => $dir,
        '#id' => 'id-' . rand(100, 10000),
        '#attributes' => [
          'class' => [
            'mb-2',
          ],
        ],
        '#items' => $link_items,
      ];
    }

    // 3 - Offset
    $items['bootstrap_dropdown']['content']['variations']['offset'] = [
      'title' => [
        '#markup' => "<h6>Offset</h6>",
      ],
      '#type' => 'container',
      '#attributes' => ['class' => ['mb-5']],
    ];

    $items['bootstrap_dropdown']['content']['variations']['offset']['example'] = [
      '#theme' => 'bootstrap_dropdown',
      '#title' => "Dropdown offset 50,10",
      '#id' => 'id-' . rand(100, 10000),
      '#offset' => '50,10',
      '#attributes' => [
        'class' => [
          'mb-2',
        ],
      ],
      '#items' => $link_items,
    ];

    // 4 - Auto close
    $autoclose = [
      'true',
      'inside',
      'outside',
      'false',
    ];

    $items['bootstrap_dropdown']['content']['variations']['autoclose'] = [
      'title' => [
        '#markup' => "<h6>Auto-close</h6>",
      ],
      '#type' => 'container',
      '#attributes' => ['class' => ['mb-5']],
    ];

    foreach ($autoclose as $config) {
      $items['bootstrap_dropdown']['content']['variations']['autoclose'][$config] = [
        '#theme' => 'bootstrap_dropdown',
        '#title' => "Dropdown autoclose {$config}",
        '#auto_close' => $config,
        '#id' => 'id-' . rand(100, 10000),
        '#attributes' => [
          'class' => [
            'mb-2',
          ],
        ],
        '#items' => $link_items,
      ];
    }

    return $items;
  }

  /**
   * Buttons component generator.
   *
   * @return array
   *   Items with new elements.
   */
  private function bsButtons() {

    $items = [];

    $types = [
      'default' => 'Default',
      'outline' => 'Outline',
    ];

    $colors = BootstrapComponentsToolkitColor::getButtonScheme();

    $sizes = [
      'sm',
      'default',
      'lg',
    ];

    $items['bootstrap_buttons'] = [
      'title' => $this->t('Buttons'),
      'content' => ['#type' => 'container'],
      'group' => $this->t('Bootstrap Components Toolkit'),
    ];

    foreach ($types as $type_key => $type) {
      $items['bootstrap_buttons']['content'][$type_key] = [
        'title' => [
          '#markup' => "<h6>{$type}</h6>",
        ],
        '#type' => 'container',
        '#attributes' => ['class' => ['mb-5']],
      ];
      foreach ($colors as $color) {
        $items['bootstrap_buttons']['content'][$type_key][$color] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['mb-2']],
        ];
        foreach ($sizes as $size) {
          $items['bootstrap_buttons']['content'][$type_key][$color]["{$type_key}_{$color}_{$size}"] = [
            '#theme' => 'bootstrap_button',
            '#button_content' => "Button {$type_key} {$color} {$size}",
            '#type' => $type_key === 'default' ? FALSE : $type_key,
            '#color' => $color,
            '#size' => $size === 'default' ? FALSE : $size,
          ];
        }
      }
    }

    return $items;
  }

  /**
   * Alert component generator.
   *
   * @return array
   *   Items with new elements.
   */
  private function bsAlert() {

    $items = [];

    $colors = BootstrapComponentsToolkitColor::COMMON_SCHEME;

    $items['bootstrap_alerts'] = [
      'title' => $this->t('Alert'),
      'content' => ['#type' => 'container'],
      'group' => $this->t('Bootstrap Components Toolkit'),
    ];

    foreach ($colors as $color) {
      $items['bootstrap_alerts']['content'][$color] = [
        '#theme' => 'bootstrap_alert',
        '#alert_content' => $this->generator->paragraphs(1),
        '#type' => $color,
        '#dismissible' => (bool) rand(0, 1),
      ];
    }

    return $items;
  }

  /**
   * Alert component generator.
   *
   * @return array
   *   Items with new elements.
   */
  private function bsBadge() {

    $items = [];

    $colors = BootstrapComponentsToolkitColor::getButtonScheme();

    $items['bootstrap_badge'] = [
      'title' => $this->t('Badge'),
      'content' => ['#type' => 'container'],
      'group' => $this->t('Bootstrap Components Toolkit'),
    ];

    $items['bootstrap_badge']['content']['default'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['mb-4'],
      ],
    ];

    $items['bootstrap_badge']['content']['rounded'] = [
      '#type' => 'container',
    ];

    foreach ($colors as $color) {
      $items['bootstrap_badge']['content']['default'][$color] = [
        '#theme' => 'bootstrap_badge',
        '#badge_content' => $this->generator->words(3),
        '#type' => $color,
      ];
    }

    foreach ($colors as $color) {
      $items['bootstrap_badge']['content']['rounded'][$color] = [
        '#theme' => 'bootstrap_badge',
        '#badge_content' => $this->generator->words(3),
        '#rounded' => TRUE,
        '#type' => $color,
      ];
    }

    return $items;
  }

  /**
   * Card component generator.
   *
   * @return array
   *   Items with new elements.
   */
  private function bsCard() {

    $items = [];

    $items['bootstrap_card'] = [
      'title' => $this->t('Card'),
      'content' => ['#type' => 'container'],
      'group' => $this->t('Bootstrap Components Toolkit'),
    ];

    $items['bootstrap_card']['content'] = [
      '#theme' => 'bootstrap_card',
      '#card_content' => [
        'title' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $this->generator->words(rand(3, 7)),
        ],
        'content' => $this->generator->paragraphs(1),
      ],
    ];

    return $items;
  }

  /**
   * Tabs component generator.
   *
   * @return array
   *   Items with new elements.
   */
  private function bsTabs() {

    $items = [];

    $items['bootstrap_tabs'] = [
      'title' => $this->t('Tabs'),
      'content' => ['#type' => 'container'],
      'group' => $this->t('Bootstrap Components Toolkit'),
    ];

    $items['bootstrap_tabs']['content'] = [
      '#theme' => 'bootstrap_tabs',
      '#nav_items' => [
        [
          'title' => 'First item',
          'id' => 'firstitem'
        ],
        [
          'title' => 'Second item',
          'id' => 'seconditem'
        ]
      ],
      '#nav_classes' => [],
      '#nav_alignment' => 'center',
      '#tabs' => [
        [
          'id' => 'firstitem',
          'content' => $this->generator->paragraphs(3),
        ],
        [
          'id' => 'seconditem',
          'content' => $this->generator->paragraphs(3),
        ]
      ]
    ];

    return $items;
  }

  /**
   * Accordion component generator.
   *
   * @return array
   *   Items with new elements.
   */
  private function bsAccordion() {

    $items = [];

    $items['bootstrap_accordion'] = [
      'title' => $this->t('Accordion'),
      'content' => ['#type' => 'container'],
      'group' => $this->t('Bootstrap Components Toolkit'),
    ];

    $items['bootstrap_accordion']['content'] = [
      '#theme' => 'bootstrap_accordion',
      '#id' => 'accordion-parent',
      '#style' => 'flush',
      '#items' => [
        [
          'parent_id' => 'accordion-parent',
          'item_id' => 'firstitem',
          'is_open' => FALSE,
          'always_open' => FALSE,
          'header_text' => 'First item',
          'header_tag' => '',
          'body_content' => [
            [
              'content' => $this->generator->paragraphs(3),
            ],
            [
              'content' => $this->generator->paragraphs(3),
            ],
          ],
        ],
        [
          'parent_id' => 'accordion-parent',
          'item_id' => 'seconditem',
          'is_open' => TRUE,
          'always_open' => FALSE,
          'header_text' => 'Second item',
          'header_tag' => '',
          'body_content' => [
            [
              'content' => $this->generator->paragraphs(3),
            ],
          ],
        ],
        [
          'parent_id' => 'accordion-parent',
          'item_id' => 'thirditem',
          'is_open' => FALSE,
          'always_open' => FALSE,
          'header_text' => 'Third item',
          'header_tag' => '',
          'body_content' => [
            [
              'content' => $this->generator->paragraphs(4),
            ],
          ],
        ],
      ],
    ];

    return $items;
  }

  /**
   * Figure component generator.
   *
   * @return array
   *   Items with new elements.
   */
  private function bsFigure() {

    $items = [];

    $items['bootstrap_figure'] = [
      'title' => $this->t('Figure'),
      'content' => ['#type' => 'container'],
      'group' => $this->t('Bootstrap Components Toolkit'),
    ];

    $items['bootstrap_figure']['content'][] = [
      '#theme' => 'bootstrap_figure',
      '#image_url' => \Drupal::service('file_url_generator')->generateAbsoluteString($this->generator->image('figure', 'jpg', '800x450', '800x450')),
      '#caption' => $this->generator->words(5),
    ];

    $items['bootstrap_figure']['content'][] = [
      '#theme' => 'bootstrap_figure',
      '#image_url' => \Drupal::service('file_url_generator')->generateAbsoluteString($this->generator->image('figure', 'jpg', '800x450', '800x450')),
      '#caption' => $this->generator->words(5),
      '#rounded' => TRUE,
      '#caption_alignment' => 'end',
    ];

    return $items;
  }

}

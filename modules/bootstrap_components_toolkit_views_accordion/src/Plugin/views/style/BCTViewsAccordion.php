<?php

namespace Drupal\bootstrap_components_toolkit_views_accordion\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Boostrap accordion style plugin.
 *
 * @ViewsStyle(
 *   id = "bct_views_accordion_style",
 *   title = @Translation("Boostrap accordion"),
 *   help = @Translation("Displays rows in a Bootstrap Accordion."),
 *   theme = "views_style_bct_accordion",
 *   display_types = {"normal"}
 * )
 */
class BCTViewsAccordion extends StylePluginBase {
  /**
   * Does the style plugin for itself support to add fields to it's output.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['panel_title_field'] = ['default' => NULL];
    $options['header_html_tag'] = ['default' => NULL];
    $options['wrapper_class'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    if (isset($form['grouping'])) {
      unset($form['grouping']);

      $form['panel_title_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Panel title field'),
        '#options' => $this->displayHandler->getFieldLabels(TRUE),
        '#required' => TRUE,
        '#default_value' => $this->options['panel_title_field'],
        '#description' => $this->t('Select the field that will be used as the accordion panel titles.'),
      ];
    }
    $form['uses_fields']['#readonly'] = TRUE;
    $form['wrapper_class'] = [
      '#title' => $this->t('Wrapper class'),
      '#description' => $this->t('The class to provide on the wrapper, outside the list.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['wrapper_class'],
    ];
    $form['header_html_tag'] = [
      '#type' => 'select',
      '#title' => $this->t('Accordion header tag'),
      '#description' => $this->t('HTML tag to wrap the accordion header, defaults to div.'),
      '#options' => [
        '' => $this->t('- Use default -'),
        'div' => 'DIV',
        'h1' => 'H1',
        'h2' => 'H2',
        'h3' => 'H3',
        'h4' => 'H4',
        'h5' => 'H5',
        'h6' => 'H6',
      ],
      '#default_value' => $this->options['header_html_tag'],
    ];
    $form['always_open'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always open'),
      '#description' => $this->t('Make accordion items stay open when another item is opened.'),
      '#default_value' => $this->options['always_open'],
    ];
    $options_select = [
      '0' => $this->t('Collapsed'),
      '1' => $this->t('Uncollapsed'),
    ];
    $form['collapse'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Collapse options'),
    ];
    $form['collapse']['first'] = [
      '#type' => 'select',
      '#title' => $this->t('First element'),
      '#options' => $options_select,
      '#default_value' => $this->options['collapse']['first'],
      '#description' => $this->t('To collapse/uncollapse the first element of the list. If there is only one item, first element settings prevails over the others (middle, last)'),
    ];
    $form['collapse']['middle'] = [
      '#type' => 'select',
      '#title' => $this->t('Middle elements'),
      '#options' => $options_select,
      '#default_value' => $this->options['collapse']['middle'],
      '#description' => $this->t('To collapse/uncollapse the middle elements of the list.'),
    ];
    $form['collapse']['last'] = [
      '#type' => 'select',
      '#title' => $this->t('Last element'),
      '#options' => $options_select,
      '#default_value' => $this->options['collapse']['last'],
      '#description' => $this->t('To collapse/uncollapse the last element of the list.'),
    ];
  }

}

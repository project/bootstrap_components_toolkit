# Bootstrap Components Toolkit

This module is a developer-oriented set of tools that help with front-end development on Bootstrap-based themes.

It provides implementations of common Bootstrap 5 components and patterns such as cards, tabs, buttons, etc... using Drupal techniques.

It was devised for and integrate specially well with [Radix](https://www.drupal.org/project/radix) and [Components!](https://www.drupal.org/project/components) and has support for Styleguide and External-use Icons.

## How to use

### Directly on code

Each component is provided at least as a [theme hook](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21theme.api.php/group/themeable/8.2.x#sec_theme_hooks) that you can use on your twig templates or as a render array. For example:

```php
  $items['bootstrap_card'] = [
    '#theme' => 'bootstrap_card',
    '#card_content' => [
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $this->generator->words(rand(3, 7)),
      ],
      'content' => $this->generator->paragraphs(1),
    ],
  ];
```

...or, if using twig and Components! (namespace is `@bootstrap_components_toolkit`):

```php
  {% include "@bootstrap_components_toolkit/bootstrap-tabs.html.twig" with {
      nav_alignment: 'center',
      tabs_alignment: is_vertical ? 'vertical'
  } %}
```

### As field formatters (atomic components)

Simpler, atomic components like buttons or badges are also provided as Field Formatters.

This kind of components can be easily tied 1:1 to some field types (i.e, buttons to link or mail fields and badges to plain text fields).

Field formatters help easing development by making components directly accessible using the UI:

## Support table

Below is a table indicating the currently supported Bootstrap components and their implementations.

<table>
  <tr>
    <td>Component</td>
    <td>Theme hook + twig template</td>
    <td>Field formatter</td>
    <td>Field types supported</td>
    <td>Views style</td>
  </tr>
  <tr>
    <td><a href="https://getbootstrap.com/docs/5.0/components/buttons/">Buttons</a></td>
    <td>✓</td>
    <td>✓</td>
    <td>link, email, file</td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://getbootstrap.com/docs/5.0/components/badge/">Badge</a></td>
    <td>✓</td>
    <td>✓</td>
    <td>link, string, list_string, string_long, entity_reference</td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://getbootstrap.com/docs/5.0/components/dropdown/">Dropdowns</a></td>
    <td>✓</td>
    <td>✓</td>
    <td>link</td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://getbootstrap.com/docs/5.0/content/figures/">Figures</a></td>
    <td>✓</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://getbootstrap.com/docs/5.0/components/alert/">Alert</a></td>
    <td>✓</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://getbootstrap.com/docs/5.0/components/card/">Card</a></td>
    <td>✓</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://getbootstrap.com/docs/5.0/components/navs-tabs/">Nav</a></td>
    <td>✓</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://getbootstrap.com/docs/5.0/components/navs-tabs/#tabs">Tabs</a></td>
    <td>✓</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><a href="https://getbootstrap.com/docs/5.0/components/accordion/">Accordion</a></td>
    <td>✓</td>
    <td></td>
    <td></td>
    <td>✓</td>
  </tr>
  <tr>
    <td><a href="https://getbootstrap.com/docs/5.0/components/accordion/">Accordion item</a></td>
    <td>✓</td>
    <td></td>
    <td></td>
    <td>✓</td>
  </tr>

</table>

## Javascript

Each component will independently load its Javascript dependencies, if any. For example, the tabs component loads bootstrap's tab.js.

This means you don't need to load the whole Bootstrap bundle. This help improve performance by only loading the needed assets per page.

If using Radix, be sure to comment out the unneeded assets on `/src/js/_bootstrap.js` to avoid duplication.

## Integrations

- [Styleguide](https://www.drupal.org/project/styleguide) - Outputs a test of each component variation to the styleguide
- [External-use icons](https://www.drupal.org/project/ex_icons) Allows to introduce Icons on the Bootstrap Button field formatter

## TODO

- Field formatter for links as streteched links.
- Generalize and improve js bundler.
- Refactor and generalize twig attributes helper.

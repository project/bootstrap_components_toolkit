const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    tab: './js/src/tab.js',
  },
  output: {
    path: path.resolve(__dirname, 'js/dist'),
    filename: '[name].js',
  },
};

<?php

namespace Drupal\bootstrap_components_toolkit;

/**
 * Bootstrap-based color/type schemes as constants for easier reuse.
 */
class BootstrapComponentsToolkitColor {

  /**
   * The primary type.
   */
  const PRIMARY = 'primary';

  /**
   * The psecondary type.
   */
  const SECONDARY = 'secondary';

  /**
   * The success type.
   */
  const SUCCESS = 'success';

  /**
   * The danger type.
   */
  const DANGER = 'danger';

  /**
   * The warning type.
   */
  const WARNING = 'warning';

  /**
   * The info type.
   */
  const INFO = 'info';

  /**
   * The light type.
   */
  const LIGHT = 'light';

  /**
   * The dark type.
   */
  const DARK = 'dark';

  /**
   * The link type.
   */
  const LINK = 'link';

  /**
   * The types shared by all components.
   */
  const COMMON_SCHEME = [
    self::PRIMARY,
    self::SECONDARY,
    self::SUCCESS,
    self::DANGER,
    self::WARNING,
    self::INFO,
    self::LIGHT,
    self::DARK,
  ];

  /**
   * The types shared by all components, as key=>value pairs.
   */
  const COMMON_SCHEME_PAIRED = [
    self::PRIMARY => 'Primary',
    self::SECONDARY => 'Secondary',
    self::SUCCESS => 'Success',
    self::DANGER => 'Danger',
    self::WARNING => 'Warning',
    self::INFO => 'Info',
    self::LIGHT => 'Light',
    self::DARK => 'Dark',
  ];

  /**
   * Returns the common scheme.
   *
   * This scheme is shared by most components on bootstrap.
   */
  public static function getCommonScheme() {
    return self::COMMON_SCHEME;
  }

  /**
   * Returns the common scheme as key_>value pairs.
   *
   * This scheme is shared by most components on bootstrap.
   */
  public static function getPairedCommonScheme() {
    return self::COMMON_SCHEME_PAIRED;
  }

  /**
   * Returns the button color scheme.
   */
  public static function getButtonScheme() {
    return array_merge(self::COMMON_SCHEME, [self::LINK]);
  }

  /**
   * Returns the button color scheme as key_>value pairs.
   */
  public static function getPairedButtonScheme() {
    return array_merge(self::COMMON_SCHEME_PAIRED, [self::LINK => 'Link']);
  }

}

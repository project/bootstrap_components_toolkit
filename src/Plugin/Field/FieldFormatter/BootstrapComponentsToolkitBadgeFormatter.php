<?php

namespace Drupal\bootstrap_components_toolkit\Plugin\Field\FieldFormatter;

use Drupal\bootstrap_components_toolkit\BootstrapComponentsToolkitColor;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Url;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Plugin implementation of the 'Bootstrap Badge Button' formatter.
 *
 * @FieldFormatter(
 *   id = "bootstrap_components_toolkit_bootstrap_badge",
 *   label = @Translation("Bootstrap Badge"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "list_string",
 *     "string_long",
 *     "entity_reference"
 *   }
 * )
 */
class BootstrapComponentsToolkitBadgeFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'type' => 'primary',
      'rounded' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $this->getSetting('type'),
      '#options' => BootstrapComponentsToolkitColor::getPairedCommonScheme(),
    ];

    $elements['rounded'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Rounded'),
      '#default_value' => $this->getSetting('rounded'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Type: @type', ['@type' => $this->getSetting('type') ? $this->getSetting('type') : 'primary']);
    $summary[] = $this->t('Rounded: @rounded', ['@rounded' => (bool) $this->getSetting('rounded') ? 'yes' : 'no']);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      /** @var \Drupal\Core\Field\FieldItemInterface $item */
      $item->getValue('uri');

      $element[$delta] = [
        '#theme' => 'bootstrap_badge',
        '#type' => (bool) $this->getSetting('type') ? $this->getSetting('type') : FALSE,
        '#rounded' => (bool) $this->getSetting('rounded'),
        '#badge_content' => $this->getContent($item),
        '#url' => $this->getUrl($item),
      ];
    }

    return $element;
  }

  /**
   * Gets the badge content.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   A field item.
   *
   * @return string
   *   The badge content.
   */
  protected function getContent(FieldItemInterface $item): string {
    if ($item instanceof EntityReferenceItem) {
      $entity = $item->entity;
      if ($entity) {
        return $entity->label();
      }
    } else {
      $value = $item->getValue();
      if (!empty($value['title'])) {
        return $value['title'];
      } else {
        return $value['value'];
      }
    }
  }

  /**
   * Get the url, if present.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   A field item.
   *
   * @return \Drupal\Core\Url|null
   *   A Url Objet or null if not found.
   */
  protected function getUrl(FieldItemInterface $item): ?Url {
    if ($item instanceof EntityReferenceItem) {
      $entity = $item->entity;
      if ($entity) {
        return $entity->toUrl();
      }
    } else {
      $value = $item->getValue();
      return !empty($value['uri']) ? Url::fromUri($value['uri']) : NULL;
    }
  }
}

<?php

namespace Drupal\bootstrap_components_toolkit\Plugin\Field\FieldFormatter;

use Drupal\Core\Url;
use Drupal\Core\Field\FormatterBase;
use Drupal\bootstrap_components_toolkit\BootstrapComponentsToolkitColor;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Bootstrap Dropdown' formatter.
 *
 * @FieldFormatter(
 *   id = "bootstrap_components_toolkit_bootstrap_dropdown_button",
 *   label = @Translation("Bootstrap Dropdown"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class BootstrapComponentsToolkitDropdownFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The epath validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Construct a MyFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   Path validator service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, PathValidatorInterface $path_validator) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->pathValidator = $path_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      // Add any services you want to inject here.
      $container->get('path.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'type' => FALSE,
      'color' => 'primary',
      'size' => FALSE,
      'title' => 'Bootstrap Dropdown',
      'url' => FALSE,
      'direction' => FALSE,
      'offset_x' => FALSE,
      'offset_y' => FALSE,
      'auto_close' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['title'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Title'),
      '#default_value' => $this->getSetting('title'),
    ];

    $elements['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title link'),
      '#default_value' => $this->getSetting('url'),
      '#description' => $this->t('Optionally, a link for the title'),
    ];

    $elements['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $this->getSetting('type'),
      '#options' => [
        FALSE => $this->t('Default'),
        'outline' => $this->t('Outline'),
      ],
    ];

    $elements['color'] = [
      '#type' => 'select',
      '#title' => $this->t('Color'),
      '#default_value' => $this->getSetting('color'),
      '#options' => BootstrapComponentsToolkitColor::getPairedButtonScheme(),
    ];

    $elements['size'] = [
      '#type' => 'select',
      '#title' => $this->t('Size'),
      '#default_value' => $this->getSetting('size'),
      '#options' => [
        FALSE => $this->t('Normal'),
        'lg' => $this->t('Large'),
        'sm' => $this->t('Small'),
      ],
    ];

    $elements['direction'] = [
      '#type' => 'select',
      '#title' => $this->t('Direction'),
      '#default_value' => $this->getSetting('direction'),
      '#options' => [
        FALSE => $this->t('Default (down)'),
        'up' => $this->t('Up'),
        'end' => $this->t('Right (end)'),
        'start' => $this->t('Left (start)'),
      ],
    ];

    $elements['offset_x'] = [
      '#type' => 'number',
      '#title' => $this->t('Offset X'),
      '#default_value' => $this->getSetting('offset_x'),
    ];

    $elements['offset_y'] = [
      '#type' => 'number',
      '#title' => $this->t('Offset Y'),
      '#default_value' => $this->getSetting('offset_y'),
    ];

    $elements['auto_close'] = [
      '#type' => 'select',
      '#title' => $this->t('Auto close behavior'),
      '#default_value' => $this->getSetting('auto_close'),
      '#options' => [
        'true' => $this->t('Default'),
        'outside' => $this->t('Outside'),
        'inside' => $this->t('Inside'),
        'false' => $this->t('False'),
      ],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Title: @title', ['@title' => $this->getSetting('title')]);
    $summary[] = $this->t('Linked: @has_url', ['@has_url' => $this->getUrl() ? 'true' : 'false']);
    $summary[] = $this->t('Type: @type', ['@type' => $this->getSetting('type') ? $this->getSetting('type') : 'default']);
    $summary[] = $this->t('Color: @color', ['@color' => $this->getSetting('color') ? $this->getSetting('color') : 'primary']);
    $summary[] = $this->t('Size: @size', ['@size' => $this->getSetting('size') ? $this->getSetting('size') : 'default']);
    $summary[] = $this->t('Direction: @dir', ['@dir' => $this->getSetting('direction') ? $this->getSetting('direction') : 'default']);
    $summary[] = $this->t('Offset: @offset', ['@offset' => $this->getOffset() ? $this->getOffset() : 'default']);
    $summary[] = $this->t('Auto close: @autoclose', ['@autoclose' => $this->getSetting('autoclose') ? $this->getSetting('autoclose') : 'default']);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $url = $this->getUrl();
    $offset = $this->getOffset();

    $element = [
      '#theme' => 'bootstrap_dropdown',
      '#type' => (bool) $this->getSetting('type') ? $this->getSetting('type') : FALSE,
      '#color' => (bool) $this->getSetting('color') ? $this->getSetting('color') : FALSE,
      '#size' => (bool) $this->getSetting('size') ? $this->getSetting('size') : FALSE,
      '#title' => $this->getSetting('title'),
      '#url' => $url,
      '#direction' => FALSE,
      '#offset' => $offset,
      '#auto_close' => (bool) $this->getSetting('auto_close') ? $this->getSetting('auto_close') : FALSE,
    ];

    foreach ($items as $delta => $item) {
      $element['#items'][$delta] = [
        '#type' => 'link',
        '#title' => $item->get('title')->getValue(),
        '#url' => Url::fromUri($item->get('uri')->getValue()),
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->isMultiple();
  }

  /**
   * Get the user-defined URL.
   *
   * @return \Drupal\Core\Url|null
   *   The URL or null if not present.
   */
  protected function getUrl(): ?Url {
    $raw_url = $this->getSetting('url');
    $result = NULL;

    if (!empty($raw_url)) {
      $url_object = $this->pathValidator->getUrlIfValid($raw_url);

      if ($url_object instanceof Url) {
        $url_object->setAbsolute();
        $result = $url_object;
      }
    }

    return $result;
  }

  /**
   * Get the user-defined offset.
   *
   * @return string|null
   *   A string representing the offset or null if not found or incomplete.
   */
  protected function getOffset(): ?string {
    $offset_x = (int) $this->getSetting('offset_x');
    $offset_y = (int) $this->getSetting('offset_y');

    if ((bool) $offset_x || (bool) $offset_y) {
      return "$offset_x, $offset_y";
    }
    else {
      return NULL;
    }
  }

}

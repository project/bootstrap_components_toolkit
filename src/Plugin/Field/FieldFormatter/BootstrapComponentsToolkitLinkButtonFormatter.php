<?php

namespace Drupal\bootstrap_components_toolkit\Plugin\Field\FieldFormatter;

use Drupal\Core\Url;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\bootstrap_components_toolkit\BootstrapComponentsToolkitColor;

/**
 * Plugin implementation of the 'Bootstrap Link Button' formatter.
 *
 * @FieldFormatter(
 *   id = "bootstrap_components_toolkit_bootstrap_link_button",
 *   label = @Translation("Bootstrap Link Button"),
 *   field_types = {
 *     "link",
 *     "email",
 *     "file"
 *   }
 * )
 */
class BootstrapComponentsToolkitLinkButtonFormatter extends FormatterBase {


  protected $type;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->type = $field_definition->getType();
  }


  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'type' => FALSE,
      'color' => 'primary',
      'size' => FALSE,
      'text' => ''
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#default_value' => $this->getSetting('type'),
      '#options' => [
        FALSE => $this->t('Normal'),
        'outline' => $this->t('Outline'),
      ],
    ];

    $elements['color'] = [
      '#type' => 'select',
      '#title' => $this->t('Color'),
      '#default_value' => $this->getSetting('color'),
      '#options' => BootstrapComponentsToolkitColor::getPairedButtonScheme(),
    ];

    $elements['size'] = [
      '#type' => 'select',
      '#title' => $this->t('Size'),
      '#default_value' => $this->getSetting('size'),
      '#options' => [
        FALSE => $this->t('Normal'),
        'lg' => $this->t('Large'),
        'sm' => $this->t('Small'),
      ],
    ];

    if ($this->type === 'email' || ($this->type === 'link' && $this->fieldDefinition->getSetting('title') <= 1) ) {
      $elements['text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Text'),
        '#default_value' => $this->getSetting('text'),
        '#required' => $this->type === 'email' ? TRUE : FALSE,
        '#description' => $this->t('Provide a default text for the button')
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Type: @type', ['@type' => $this->getSetting('type') ? $this->getSetting('type') : 'default']);
    $summary[] = $this->t('Color: @color', ['@color' => $this->getSetting('color') ? $this->getSetting('color') : 'primary']);
    $summary[] = $this->t('Size: @size', ['@size' => $this->getSetting('size') ? $this->getSetting('size') : 'default']);
    if ($this->type === 'email') {
      $summary[] = $this->t('Button text: @text', ['@text' => $this->getSetting('text') ? $this->getSetting('text') : 'empty']);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#theme' => 'bootstrap_button',
        '#type' => (bool) $this->getSetting('type') ? $this->getSetting('type') : FALSE,
        '#color' => (bool) $this->getSetting('color') ? $this->getSetting('color') : FALSE,
        '#size' => (bool) $this->getSetting('size') ? $this->getSetting('size') : FALSE,
        '#button_content' => $this->getButtonContent($item),
        '#url' => $this->getButtonUrl($item),
      ];
    }

    return $element;
  }

  /**
   * Gets the button text from each supported field type.
   *
   * For mail field types, as they don't support text, we prompt the user
   * on the config form and use it.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item.
   *
   * @return string
   *   The button text.
   */
  protected function getButtonContent(FieldItemInterface $item): string {
    $result = '';

    switch($this->type) {
      case "email":
        $result = $this->t($this->getSetting('text'));
        break;
      case "file":
        // Gets either the description or the filename as a fallback.
        /** @var \Drupal\file\Plugin\Field\FieldType\FileItem $item */
        $description = $item->get('description')->getValue();
        $result = $description ? $description : $item->getEntity()->label();
        break;
      default:
        if (!empty($item->get('title')->getValue())) {
          $result = $item->get('title')->getValue();
        }
        elseif ($this->fieldDefinition->getSetting('title') <= 1 && !empty($this->getSetting('text'))) {
          $result = $this->t($this->getSetting('text'));
        }
        elseif (!$item->isExternal()) {
          $params = Url::fromUri($item->getValue()['uri'])->getRouteParameters();
          $entity_type = key($params);
          $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);
          $result = $entity->label();
        }
        else {
          $result = $item->getUrl()->toString();
        }
    }

    return $result;
  }

  /**
   * Gets the button target URL.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item.
   *
   * @return \Drupal\Core\Url
   *   The button target url.
   */
  protected function getButtonUrl(FieldItemInterface $item): Url {

    $result = '';

    switch($this->type) {
      case "email":
        $result = Url::fromUri('mailto:' . $item->value);
        break;
      case "file":
        /** @var \Drupal\file\Plugin\Field\FieldType\FileItem $item */
        $file = $item->entity;
        $result = \Drupal::service('file_url_generator')->generate($file->getFileUri());
        break;
      default:
        $result = Url::fromUri($item->get('uri')->getValue());
    }

    return $result;
  }

}
